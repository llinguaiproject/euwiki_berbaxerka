import os

# Gure kodearen karpetaren ibilbide osoa aterako dugu
kodearen_karpeta = os.path.dirname(os.path.abspath(__file__))

datuen_karpeta = os.path.join(kodearen_karpeta, "..", "data")
selenium_karpeta = os.path.join(kodearen_karpeta, "..", "geckodriver")
txostenen_karpeta = os.path.join(kodearen_karpeta, "..", "txostenak")


# Sistemako ibilbideetara gure geckodriver-a gehitu
os.environ["PATH"] = os.environ["PATH"]+":{}".format(selenium_karpeta)
os.environ["PATH"] = os.environ["PATH"]+":{}/geckodriver".format(selenium_karpeta)

BERBAXERKA_WEB = os.environ.get("BERBAXERKA_WEB", "https://berbaxerka.gitlab.io/")


def atera_argazkia(non_gorde: str, izena: str):
    os.system("import -window root -resize 1240x768 -delay 200 {}".format(os.path.join(non_gorde, izena)))
