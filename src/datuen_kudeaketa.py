"""Wikipediako datuak irakurtzeko programa. Horrez gain, gaurko hizkiekin balizkoak
diren elementuak ateratzen ditu berbaxerkan idazteko."""
import os
import string
import json

from wiki_dump_reader import Cleaner, iterate

from src import datuen_karpeta


def kendu_puntuazioa(testua: str) -> str:
    """
    >>> kendu_puntuazioa("kaixo.,")
    'kaixo  '
    """
    testua = testua.lower()
    for char in string.punctuation:
        testua = testua.replace(char, " ")
    return testua


def hitza_egokia_da(hitza, gaurko_hizkiak, nahitaezkoa):
    if len(hitza) < 3:
        return False
    if not set(hitza).issubset(set(gaurko_hizkiak)):
        return False
    # Gutxienez bokal bat izan beharko luke
    if not any([bokala in hitza for bokala in ["a", "e", "i", "o", "u"]]):
        return False
    if nahitaezkoa not in hitza:
        return False
    for hizkia in gaurko_hizkiak:
        if hizkia in hitza:
            return True
    return False


def atxiki_wikpediako_hitzen_maiztasuna(fitxategiaren_izena: str = "data"):
    garbitzailea = Cleaner()
    artikuluen_indizea = 0
    for izenburua, testua in iterate(os.path.join(datuen_karpeta, fitxategiaren_izena)):
        testu_erauzia = garbitzailea.clean_text(testua)
        testua_puntuazio_markekin, _ = garbitzailea.build_links(testu_erauzia)
        testu_garbia = kendu_puntuazioa(testua).split(' ')
        artikuluko_hitzen_maiztasuna = {
            hitza: testu_garbia.count(hitza) for hitza in set(testu_garbia) if hitza and len(hitza) > 2
        }
        with open(os.path.join(datuen_karpeta, "bitartekariak", "{}.json".format(artikuluen_indizea)), 'w') as f:
            json.dump(artikuluko_hitzen_maiztasuna, f, indent=2)
        artikuluen_indizea += 1


def bateratu_artikuluen_maiztasunak():
    hitzen_maiztasuna = {}
    json_fitxategiak = os.listdir(os.path.join(datuen_karpeta, "bitartekariak"))
    artikulu_kopurua = len(json_fitxategiak)
    for json_fitxategia in json_fitxategiak:
        with open(os.path.join(datuen_karpeta, "bitartekariak", json_fitxategia), 'r') as f:
            artikuluko_hitzen_maiztasuna = json.load(f)
            for hitza, maiztasuna in artikuluko_hitzen_maiztasuna.items():
                hitzen_maiztasuna[hitza] = hitzen_maiztasuna.get(hitza, 0) + artikuluko_hitzen_maiztasuna[hitza]
    for hitza in hitzen_maiztasuna.keys():
        if hitzen_maiztasuna.get(hitza) == artikulu_kopurua:
            # Seguruenik metadaturen bat izango da, beraz ezabatu
            hitzen_maiztasuna.pop(hitza)
    with open(os.path.join(datuen_karpeta, "hitzen_maiztasuna.json"), 'w') as f:
        json.dump(hitzen_maiztasuna, f, indent=2)


def iragazi_maiztasunen_hiztegia(gaurko_hizkiak, nahitaezkoa):
    with open(os.path.join(datuen_karpeta, "hitzen_maiztasuna.json"), 'r') as f:
        maiztasunen_hiztegia = json.load(f)
        for hitza in list(maiztasunen_hiztegia.keys()):
            if maiztasunen_hiztegia[hitza] < 10:  # Gutxiengo maiztasun bat nahi dugu
                maiztasunen_hiztegia.pop(hitza)
            elif not hitza_egokia_da(hitza, gaurko_hizkiak=gaurko_hizkiak, nahitaezkoa=nahitaezkoa):
                maiztasunen_hiztegia.pop(hitza)
    return maiztasunen_hiztegia


def garbitu():
    """Fitxategi bitartekariak ezabatu"""
    json_fitxategiak = os.listdir(os.path.join(datuen_karpeta, "bitartekariak"))
    for json_fitxategia in json_fitxategiak:
        os.remove(os.path.join(datuen_karpeta, "bitartekariak", json_fitxategia))
    return None


if __name__ == '__main__':
    # atxiki_wikpediako_hitzen_maiztasuna()
    bateratu_artikuluen_maiztasunak()
    # maiztasun_handieneko_hitzak = [hitza for hitza in
    #                                sorted(hitzen_maistazun_hiztegia, key=hitzen_maistazun_hiztegia.get, reverse=True)]
    #
    # lehen_mila_hitzak = maiztasun_handieneko_hitzak[:1000]

