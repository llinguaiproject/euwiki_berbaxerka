import time
from src import BERBAXERKA_WEB

from selenium import webdriver
from selenium.webdriver.common.by import By


def hasieratu_arakatzailea(itxaron_segunduak: int = 10):
    # Todo: beste nabigatzeeileekin bateragarria egin
    arakatzailea = webdriver.Firefox()
    arakatzailea.get(BERBAXERKA_WEB)
    # Itxaron web orria kargatu arte
    time.sleep(itxaron_segunduak)
    return arakatzailea


def atxiki_bidali_botoia(arakatzailea):
    return arakatzailea.find_element(by=By.ID, value="submit_button")


def atxiki_gaurko_letrak(arakatzailea):
    hizkiak = arakatzailea.find_elements(value="hexLink", by=By.CLASS_NAME)
    hizkien_mapa = {hizki.text: hizki for hizki in hizkiak}
    erdiko_hizkia = arakatzailea.find_element(by=By.ID, value="center-letter")
    hizkien_mapa[erdiko_hizkia.text] = erdiko_hizkia
    return hizkien_mapa, erdiko_hizkia.text


def itxi_arakatzailea(arakatzailea):
    arakatzailea.close()


def idatzi_hitza(hitza:str, webeko_letrak, bidali_botoia):
    for letra in hitza:
        webeko_letrak[letra].click()
        time.sleep(0.2)
    bidali_botoia.click()
    time.sleep(0.5)


def asmatutakoak_lortu(arakatzailea):
    asmatutakoak = []
    hizkiak = arakatzailea.find_elements(value="word-found", by=By.CLASS_NAME)
    for asmatutako_hizkiak in hizkiak:
        asmatutakoak.append(asmatutako_hizkiak.text)
    return asmatutakoak



if __name__ == '__main__':
    arakatzailea = hasieratu_arakatzailea()
    time.sleep(5)
    letrak, erdiko_letra = atxiki_gaurko_letrak(arakatzailea)
    bidali_botoia = atxiki_bidali_botoia(arakatzailea)
    print(letrak)
    print(erdiko_letra)
    test_hitza = ''.join(letrak.keys())
    print(test_hitza)
    idatzi_hitza(test_hitza, letrak, bidali_botoia)
    idatzi_hitza("boa", letrak, bidali_botoia)
    idatzi_hitza("birao", letrak, bidali_botoia)
    asmatutakoak = asmatutakoak_lortu(arakatzailea)
    print(asmatutakoak)
    # sartu letra bat
    time.sleep(10)
    itxi_arakatzailea(arakatzailea)

