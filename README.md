
## Instalazioa

## Euskarazko Wikipedia Jeisten

Euskarazko wikipedia jeisteko honako helbidera jo dut: https://dumps.wikimedia.org/euwiki/20220120/
Orokorrean, dumps.wikimedia.org helbidean deskarga hauek eguneratzen joaten dira, beraz, komenigarria da azken bertsioa 
jeistea.

Behin fitxategia (euwiki-20220120-pages-articles-multistream.xml.bz2) deskargatu dugunean, hau "<proiektuaren_karpeta>/data"
karpetara erauzi behar dugu.

### Geckodriver - Firefox erabiltzaileentzat
Ubunturen kasurako, honakoa egin beharko duzu

    sudo cp <proiektuaren_ibilbidea>/geckodriver/geckodriver /usr/bin

Honekin geckodriver exekutatzailea gure sistema eragileak aurki dezakeen
karpeta batean jarriko dugu.

### Imagemagic
