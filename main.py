import os
import time

from src import txostenen_karpeta, atera_argazkia

import src.web_automatizazioa as berbaxerka
import src.datuen_kudeaketa as datuak
from pprint import pformat, pprint

import datetime


def sortu_txostenaren_izena():
    gaurko_data = datetime.datetime.today()
    txostenaren_izena = "wiki_berbaxerka_{}.txt".format(gaurko_data.date())
    return txostenaren_izena


def sortu_argazkiaren_izena(zenbat_hitz: int):
    gaurko_data = datetime.datetime.today()
    txostenaren_izena = "wiki_berbaxerka_{}_{}_hitz.png".format(gaurko_data.date(), zenbat_hitz)
    return txostenaren_izena


def txostenean_idatzi(testua, txostenaren_izena):
    with open(os.path.join(txostenen_karpeta, txostenaren_izena), "a") as txostena:
        txostena.write(testua)
        pprint(testua)


def exekutatu_berbaxerka():
    txostena = sortu_txostenaren_izena()
    # Hasieratu berbaxerka:
    arakatzailea = berbaxerka.hasieratu_arakatzailea()
    webeko_letrak, erdiko_letra = berbaxerka.atxiki_gaurko_letrak(arakatzailea)
    bidali_botoia = berbaxerka.atxiki_bidali_botoia(arakatzailea)

    gaurko_hizkiak = [letra for letra in webeko_letrak]
    txostenean_idatzi(pformat('Gaurko hizkiak hauek dira: {}'.format(gaurko_hizkiak)), txostena)
    maiztasunen_hiztegia = datuak.iragazi_maiztasunen_hiztegia(gaurko_hizkiak,
                                                               nahitaezkoa=erdiko_letra)
    # Atera maiztasun handieneko hitzak
    maiztasun_handieneko_hitzak = [hitza for hitza in
                                   sorted(maiztasunen_hiztegia, key=maiztasunen_hiztegia.get, reverse=True)]
    txostenean_idatzi(
        testua="Wikipedian aurkitu den hizen kopurua: {}".format(len(maiztasunen_hiztegia)),
        txostenaren_izena=txostena
    )
    bukatu_iterazioa = False
    for hasi, bukatu in [(0, 1000), (1000, 2000), (2000, 3000), (3000, 4000)]:
        if bukatu > len(maiztasun_handieneko_hitzak):
            bukatu = len(maiztasun_handieneko_hitzak)
            bukatu_iterazioa = True

        hitzak = maiztasun_handieneko_hitzak[hasi:bukatu]
        txostenean_idatzi(
            testua=pformat("Hasiera: {}, Bukaera:{}, Hitzak: {}".format(hasi, bukatu,hitzak)),
            txostenaren_izena=txostena
        )

        for hitza in hitzak:
            berbaxerka.idatzi_hitza(hitza,
                                    webeko_letrak=webeko_letrak,
                                    bidali_botoia=bidali_botoia)
        time.sleep(2)
        atera_argazkia(txostenen_karpeta, sortu_argazkiaren_izena(bukatu))
        asmatutakoak = berbaxerka.asmatutakoak_lortu(arakatzailea)

        txostenean_idatzi(
            testua=pformat("Asmatutako hitzak: {}".format(asmatutakoak)),
            txostenaren_izena=txostena
        )
        if bukatu_iterazioa:
            break
    time.sleep(1800)
    berbaxerka.itxi_arakatzailea(arakatzailea)


if __name__ == '__main__':
    exekutatu_berbaxerka()
